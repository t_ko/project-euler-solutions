import java.lang.Math;
class p9 {
  public static void main(String[] args) {
    int target = 1000;
    int triplet[] = getPythagoreanTriplet(target);
    long product = triplet[0] * triplet[1] * triplet[2];
    System.out.println("Pythagorean triplet: " + triplet[0] + ", " + triplet[1] + ", " + triplet[2]);
    System.out.println("Corresponding product: " + product);
  }
  public static int[] getPythagoreanTriplet(int n) {
    int[] res = {0, 0, 0};
    int a = 1, b = 2, c = 3;
    while (a < n) {
      b = a + 1;
      while (b < n) {
        c = n - b - a;
        if (Math.pow(a, 2) + Math.pow(b, 2) ==  Math.pow(c, 2)) {
          res[0] = a;
          res[1] = b;
          res[2] = c;
          return res;
        }
        ++b;
      }
      ++a;
    }
    return res;
  }
}