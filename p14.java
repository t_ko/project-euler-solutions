public class p14 {
        public static void main(String[] args) {
                int n = 1000000;
                long l;
                int terms = 1;
                int max_terms = 0;
                int max_val = 0;
                while (n > 1) {
                        l = (long) n;
                        terms = 1;
                        while (l > 1) {
                                ++terms;
                                if (l % 2 == 0) { l = l / 2; }
                                else { l = 3 * l + 1; }
                        }
                        if (max_terms < terms) {
                                max_terms = terms;
                                max_val = n;
                        }
                        --n;
                }
                System.out.println(max_val + ", " + max_terms);
        }
}