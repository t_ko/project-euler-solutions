public class p5 {
        public static void main(String[] args) {
                int n = 2520;
                boolean found = false;
                do {
                        int i = 11;
                        while (i < 20) {
                                if (n % i == 0) { ++i; }
                                else { ++n; break; }
                        }
                        if (i == 20 && n % i == 0) { found = true; }
                        else { ++n; }
                } while(!found);
                System.out.println(n);
        }
}