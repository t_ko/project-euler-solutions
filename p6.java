import java.lang.Math;
public class p6 {
        public static void main(String[] args) {
                long l = 0;             // sum of squares
                long ll = 0;            // square of sums
                int sum = 0;
                for (int i = 1; i < 101; ++i) {
                        l += i * i;
                        sum += i;
                }
                ll = sum * sum;
                System.out.println(Math.abs(l - ll));
        }
}