public class p4 {
        public static void main(String[] args) {
                boolean found = false;
                int n = 998001; // initial value is the product of 999 * 999
                while (!found) {
                        if (isPalindrome(n)) {
                                if (suitableProduct(n)) {
                                        found = true;
                                        System.out.println(n);
                                }
                                else {
                                        --n;
                                }
                        }
                        else {
                                --n;
                        }
                }
        }
        public static boolean suitableProduct(int n) {
                boolean suitable = false;
                int t_b = 999;
                for (int i = 100; i < 1000; ++i) {
                        if (n % i == 0 && n / i <= t_b) {
                                suitable = true;
                                break;
                        }
                }
                return suitable;
        }
        public static boolean isPalindrome(int n) {
    String s = Integer.toString(n);
                int a = 0;
                int b = s.length() - 1;
                while (a < b) {
                        if (s.charAt(a) == s.charAt(b)) {
                                ++a;
                                --b;
                        }
                        else {
                                return false;
                        }
                }
                return true;
        }
}